Kreirati stranicu koja ima obrazac za registraciju (osnovni podaci,
datum rođenja i još neka polja), podatke slati na register.php stranicu
POST metodom. Stranica register.php proverava da li su sva polja
ispunjena ako neko nije preusmerava korisnika na početnu stranicu i
ispisuje poruku o grešci. Ukoliko je sve u redu stranica register ispisuje
sve unete podatke a za uneti datum ispisuje ime dana na engleskom
jeziku za taj datum. Unete podatke očistiti od suvišnih praznih mesta
primenom odgovarajućih funkcija.
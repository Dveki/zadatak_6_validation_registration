<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $lnameErr = $emailErr = $genderErr = $websiteErr = $bdayErr ="";
$name = $lname = $email = $gender = $comment = $website = $bday = $num1 = $num2 = $num3 = $num4 = $num5 = $num6 = $favcolor ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
    elseif(strlen($_POST["name"]) < 3  ) {
        $nameErr = "Name need to have more than 2 characters";
      }
      elseif(strlen($_POST["name"]) > 10  ) {
        $nameErr = "Name need to have less than 10 characters";
      }
      else {
        $name = test_input($_POST["name"]);
      }
  }

  if (empty($_POST["lname"])) {
    $lnameErr = "Last Name is required";
  } else {
    $lname = test_input($_POST["lname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
      $lnameErr = "Only letters and white space allowed"; 
    }
    elseif(strlen($_POST["lname"]) < 3  ) {
        $lnameErr = "Last Name need to have more than 2 characters";
      }
      elseif(strlen($_POST["lname"]) > 10  ) {
        $lnameErr = "Last Name need to have less than 10 characters";
      }
      else {
        $lname = test_input($_POST["lname"]);
      }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } 
  else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
    elseif(strlen(strstr($_POST["email"],'@', true)) < 6  ) {
      $emailErr = "Email need to have more than 6 characters";
    }
    elseif (checkEmailAdress($email)) {
      $emailErr = "Email need @ and gmail.com domain";
    }
  }
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }

  if (empty($_POST["bday"])) {
    $bdayErr = "Bday is required";
  } else {
    $bday = test_input($_POST["bday"]);
  }
  if (empty($_POST["num1"])) {
    $num1 = "";
  } else {
    $num1 = (int)test_input($_POST["num1"]);
  }
  if (empty($_POST["num2"])) {
    $num2 = "";
  } else {
    $num2 = (int)test_input($_POST["num2"]);
  }
  if (empty($_POST["num3"])) {
    $num3 = "";
  } else {
    $num3 = (int)test_input($_POST["num3"]);
  }
  if (empty($_POST["num4"])) {
    $num4 = "";
  } else {
    $num4 = (int)test_input($_POST["num4"]);
  }
  if (empty($_POST["num5"])) {
    $num5 = "";
  } else {
    $num5 = (int)test_input($_POST["num5"]);
  }
  if (empty($_POST["num6"])) {
    $num6 = "";
  } else {
    $num6 = (int)test_input($_POST["num6"]);
  }   
  if (empty($_POST["favcolor"])) {
    $favcolor = "";
  } else {
    $favcolor = test_input($_POST["favcolor"]);
  }

  if (!empty($nameErr) or !empty($lnameErr)or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr) or !empty($bdayErr)) {
    $params .= "name=" . urlencode($_POST["name"]);
    $params .= "&lname=" . urlencode($_POST["lname"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);
    $params .= "&bday=" . urlencode($_POST["bday"]);
    $params .= "&num1=" . urlencode($_POST["num1"]);
    $params .= "&num2=" . urlencode($_POST["num2"]);
    $params .= "&num3=" . urlencode($_POST["num3"]);
    $params .= "&num4=" . urlencode($_POST["num4"]);
    $params .= "&num5=" . urlencode($_POST["num5"]);
    $params .= "&num6=" . urlencode($_POST["num6"]);
    $params .= "&favcolor=" . urlencode($_POST["favcolor"]);
  

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lnameErr=" . urlencode($lnameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
    $params .= "&bdayErr=" . urlencode($bdayErr);
    
    header("Location: index1.php?" . $params);
  }  
  else {
    
    echo "<h2>Your Input:</h2>";
    echo "Full Name: " .ucfirst($_POST['name']).' '.ucfirst($_POST['lname']);
    echo "<br>";
    
    echo "Email: " . $_POST['email'];
    echo "<br>";
    
    echo "Website: " . $_POST['website'];
    echo "<br>";
    
    echo "Comment: " . $_POST['comment'];
    echo "<br>";
    
    echo "Gender: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";

    echo 'Your date of birth is: ';
    echo $bday;
    echo "<br>";
    echo 'Your day of birth is: ';
    $timestamp = strtotime($bday);
    $day = date('l', $timestamp);
    echo $day;
    echo "<br>";
  
  $broj = array($num1, $num2, $num3, $num4, $num5, $num6 );
  for ($i = 0; $i < count($broj); $i++) {
  if ($broj[$i] == '') {
  }
  else {
  /*provere za integer i string kako bi preskocio ukoliko je polje prazno, a ukolikoje 0 racunao 
  var_dump ($broj);*/
  echo 'Your array is:';
  print "<pre>"; 
  print_r($broj); 
  print "</pre>";
  
  include 'functions.php';
  echo sumbr($broj, $favcolor);
  echo maxbr($broj, $favcolor);
  echo minbr($broj, $favcolor);
  break;
  echo "<br>";
  }
} 
  echo "<a href=\"index1.php\">Return to form</a>";
  }
}

function checkEmailAdress($data) {
if (!preg_match("/@gmail.com/",$data) === true)
return true;
else
return false;
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

</body>
</html>
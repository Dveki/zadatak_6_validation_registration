<!DOCTYPE HTML>  
<html>
<head>
<link rel="stylesheet" href="style1.css">
</head>
<body>  

<?php
    $name = $lname = $email = $website = $comment = $gender = $bday = $num1 = $num2 = $num3 = $num4 = $num5 = $num6 = $favcolor ="";
    $nameErr = $lnameErr = $emailErr = $genderErr = $websiteErr = $bdayErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['lname'])) { $lname = $_GET['lname']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['website'])) { $website = $_GET['website']; }
    if (isset($_GET['comment'])) { $comment = $_GET['comment']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
    if (isset($_GET['bday'])) { $bday = $_GET['bday']; }
    if (isset($_GET['num1'])) { $num1 = $_GET['num1']; }
    if (isset($_GET['num2'])) { $num2 = $_GET['num2']; }
    if (isset($_GET['num3'])) { $num3 = $_GET['num3']; }
    if (isset($_GET['num4'])) { $num4 = $_GET['num4']; }
    if (isset($_GET['num5'])) { $num5 = $_GET['num5']; }
    if (isset($_GET['num6'])) { $num6= $_GET['num6']; }
    if (isset($_GET['favcolor'])) { $favcolor = $_GET['favcolor']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['lnameErr'])) { $lnameErr = $_GET['lnameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['websiteErr'])) { $websiteErr = $_GET['websiteErr']; }
    if (isset($_GET['commentErr'])) { $commentErr = $_GET['commentErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }
    if (isset($_GET['bdayErr'])) { $bdayErr = $_GET['bdayErr']; }

    
?>
<div class="main">
<form method="post" action="register1.php">
<fieldset class="main_field">
<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field.</span></p>
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  Last Name: <input type="text" name="lname" value="<?php echo $lname;?>">
  <span class="error">* <?php echo $lnameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Gender:
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  Birthday: <input type="date" name="bday" value="<?php echo $bday;?>">
  <span class="error">* <?php echo $bdayErr;?></span><br><br>
  </fieldset>
  <fieldset class="extra_field">
        Pick a number (between -9999 and 9999):<input type="number" name="num1" min="-9999" max="9999" value="<?php echo $num1;?>"><br/>
        Pick a number (between -9999 and 9999):<input type="number" name="num2" min="-9999" max="9999" value="<?php echo $num2;?>"><br/>
        Pick a number (between -9999 and 9999):<input type="number" name="num3" min="-9999" max="9999" value="<?php echo $num3;?>"><br/> 
        Pick a number (between -9999 and 9999):<input type="number" name="num4" min="-9999" max="9999" value="<?php echo $num4;?>"><br/>
        Pick a number (between -9999 and 9999):<input type="number" name="num5" min="-9999" max="9999" value="<?php echo $num5;?>"><br/>
        Pick a number (between -9999 and 9999):<input type="number" name="num6" min="-9999" max="9999" value="<?php echo $num6;?>"><br/>
        Please select color: <input type="color" name="favcolor" value="<?php echo $favcolor;?>"><br/>
    </fieldset>
  <input type="submit" name="submit" value="Submit">  
</form>
</div>

</body>
</html>